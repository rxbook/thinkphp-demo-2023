<?php
namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\service\Mq;

/**
 * 简单工作模式的消费者
 * @package app\command
 */
class MqDemo1 extends Command
{
    protected function configure()
    {
        $this->setName('MqDemo1')
        	->addArgument('name', Argument::OPTIONAL, "your name")
            ->addOption('city', null, Option::VALUE_REQUIRED, 'city name')
        	->setDescription('Say Hello');
    }

    protected function execute(Input $input, Output $output)
    {
        $mq = new Mq();
        $callback = function ($msg) use ($mq) {
            $body = json_decode($msg->body, true);
            var_dump($body);
            $msg->ack();
        };

        $mq->consumer($callback, 'queueDemo1');
    }
}
<?php
namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\service\Mq;

/**
 * 路由模式 info 类型的消费者
 * @package app\command
 */
class MqDemo40 extends Command
{
    protected function configure()
    {
        $this->setName('MqDemo4')
        	->addArgument('name', Argument::OPTIONAL, "your name")
            ->addOption('city', null, Option::VALUE_REQUIRED, 'city name')
        	->setDescription('Say Hello');
    }

    protected function execute(Input $input, Output $output)
    {
        $mq = new Mq();
        $callback = function ($msg) use ($mq) {
            $body = json_decode($msg->body, true);
            var_dump($body);
        };

        $mq->consumer($callback, '', 'phpdemo.direct', 'direct', 'info');
    }
}
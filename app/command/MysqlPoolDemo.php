<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\service\MysqlPool;

/**
 * Swoole Mysql数据库协程连接池
 * @package app\command
 */
class MysqlPoolDemo extends Command {
    protected function configure() {
        $this->setName('MysqlPoolDemo')
            ->addArgument('name', Argument::OPTIONAL, "your name")
            ->addOption('city', null, Option::VALUE_REQUIRED, 'city name')
            ->setDescription('Say Hello');
    }

    protected function execute(Input $input, Output $output) {
        $config = [
            'size' => 4,
            'host' => '127.0.0.1',
            'port' => 3306,
            'dbname' => 'swoole',
            'user' => 'root',
            'password' => '123456',
            'charset' => 'utf8'
        ];
        $res = MysqlPool::getInstance($config)->get();
        var_dump($res);
    }
}
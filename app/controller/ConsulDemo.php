<?php

namespace app\controller;

use app\BaseController;
use app\service\Consul;

class ConsulDemo extends BaseController {
    private $serviceId = 'rxService';

    //注册服务
    public function registerDemo() {
        $data = [
            'ID' => $this->serviceId, //服务的id
            'Name' => 'rxService', //服务的名称
            'Tags' => ['core.rx'],
            // 'Address' => '127.0.0.1', //访问的地址
            'Port' => 8087,
            'Check' => [
                'HTTP' => 'http://test-service1.cc:80',
                'Interval' => '5s', //回调时间间隔
            ]
        ];
        $consul = new Consul();
        $rs = $consul->registerService($data);
        var_dump($rs);
    }

    //服务发现
    public function serviceInfo() {
        $consul = new Consul();
        $rs = $consul->serviceInfo($this->serviceId);
        echo $rs;
    }

    //配置信息
    public function configInfo() {
        $consul = new Consul();
        $rs = $consul->configInfo($this->serviceId . '/dev/db1');
        echo $rs;
    }
}

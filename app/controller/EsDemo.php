<?php

namespace app\controller;

use app\BaseController;
use app\service\Elasticsearch;

class EsDemo extends BaseController {
    /**
     * @var Elasticsearch
     */
    private $es;

    public function initialize() {
        parent::initialize();
        $this->es = new Elasticsearch();
    }

    public function add() {
        $str = $this->es->add('demo', 'all', ['id' => 1, 'name' => 'zhangsan', 'sex' => 0], 'demo-1');
        echo json_encode($str);
    }

    public function update() {
        $str = $this->es->update('demo', 'all', ['name' => 'zhangsan1', 'sex' => 1], 'demo-1');
        echo json_encode($str);
    }

    public function delete() {
        $str = $this->es->delete('demo', 'all', 'demo-1');
        echo json_encode($str);
    }

    public function search() {
        $query = [
            "bool" => [
                "filter" => [
                    ["term" => ["id" => 1]],
                ],
            ]
        ];

        $str = $this->es->search('demo', 'all', $query, 0, 1, ["id" => 'desc']);
        echo json_encode($str);
    }
}

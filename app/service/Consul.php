<?php

namespace app\service;

use app\Http;
use think\facade\Config;

class Consul {
    private $httpUrl;

    public function __construct() {
        $consulConfig = Config::get('common.consul');
        $this->httpUrl = 'http://' . $consulConfig['host'] . ':' . $consulConfig['port'] . '/';
    }

    //服务注册 agent/service/register
    public function registerService($data) {
        $url = $this->httpUrl . 'v1/agent/service/register';
        return Http::curlPut($url, $data);
    }

    //服务信息
    public function serviceInfo($serviceId) {
        $url = $this->httpUrl . 'v1/health/service/' . $serviceId;
        return Http::curlGet($url);
    }

    //配置信息
    public function configInfo($key) {
        $url = $this->httpUrl . 'v1/kv/' . $key;
        return Http::curlGet($url);
    }


}

<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'mq-demo1' => 'app\command\MqDemo1',
        'mq-demo3' => 'app\command\MqDemo3',
        'mq-demo31' => 'app\command\MqDemo31',
        'mq-demo4' => 'app\command\MqDemo40',
        'mq-demo41' => 'app\command\MqDemo41',
        'mq-demo5' => 'app\command\MqDemo50',
        'mq-demo51' => 'app\command\MqDemo51',
        'mq-demo6' => 'app\command\MqDemo60',
        'mq-demo61' => 'app\command\MqDemo61',
        'mysql-pool-demo' => 'app\command\MysqlPoolDemo',
    ],
];

<?php

/**
 * PHP的垃圾回收机制
 * 要尽量避免循环引用的使用，因为遇到循环引用的时候它是回收不了的，只有当触发 10000次 的条件才会集中回收，并且当回收的时候PHP是卡住的，是不会往下执行的，所以就会出现非常耗时的情况。
 */
class Test {
    public $name;
}

$start_time = microtime(true);
//循环实例化1000万次
for ($i = 0; $i < 10000000; $i++) {
    $t = new Test();
    // $t->name = $t; //循环引用
    unset($t);
}
$end_time = microtime(true);
echo round(($end_time - $start_time), 4) . PHP_EOL;
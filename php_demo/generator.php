<?php

/**
 * PHP中的Generate生成器的使用
 */

function getRange1($max = 10) {
    $array = [];
    for ($i = 1; $i < $max; $i++) {
        $array[] = $i;
    }
    return $array;
}

function getRange2($max = 10) {
    for ($i = 1; $i < $max; $i++) {
        //只循环遍历值和 yield 输出。 yield 与返回值类似，因为它也是从函数返回一个值，
        //但唯一的区别是 yield 只会在需要时返回一个值，并且不会尝试将整个数据集保留在内存中。
        yield $i;
    }
}

//使用传统的for循环会报错：内存耗尽
//foreach (getRange1(PHP_INT_MAX) as $value) {
//    echo $value . PHP_EOL;
//}

//使用生成器不会报错
foreach (getRange2(PHP_INT_MAX) as $value) {
    echo $value . PHP_EOL;
}
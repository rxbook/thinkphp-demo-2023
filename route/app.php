<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});

Route::get('hello/:name', 'index/hello');

//consul
Route::get('consul/register/demo', 'ConsulDemo/registerDemo');
Route::get('consul/service/info', 'ConsulDemo/serviceInfo');
Route::get('consul/config/info', 'ConsulDemo/configInfo');

//ES
Route::get('es/add', 'EsDemo/add');
Route::get('es/update', 'EsDemo/update');
Route::get('es/delete', 'EsDemo/delete');
Route::get('es/search', 'EsDemo/search');

//MQ相关
Route::get('mq/demo1', 'MqDemo/pushDemo1');
Route::get('mq/demo3', 'MqDemo/pushDemo3');
Route::get('mq/demo4', 'MqDemo/pushDemo4');
Route::get('mq/demo5', 'MqDemo/pushDemo5');
Route::get('mq/demo6', 'MqDemo/pushDemo6');
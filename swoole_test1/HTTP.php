<?php

class HTTP {
    private $http = null;

    public function __construct() {
        $this->http = new Swoole\Http\Server('0.0.0.0', 9503);
        $this->http->set([
            'enable_static_handler' => true,
            'document_root' => "./static",
        ]);
        $this->http->on('Request', [$this, "onRequest"]);

        //启动服务器
        $this->http->start();
    }

    public function onRequest($request, $response) {
        var_dump($request->get, $request->post);


        $response->header('Content-Type', 'text/html; charset=utf-8');
        $response->end('Hello Swoole.' . json_encode($request->get));
    }
}

new HTTP();



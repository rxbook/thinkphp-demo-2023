<?php

$http = new Swoole\Http\Server('0.0.0.0', 9501);

$http->set([
    //设置启动的 Worker 进程数。【默认值：CPU 核数】
    'worker_num' => 2,

    //如果要使用 Task ，需要先设置 task_worker_num ，它代表的是开启的 Task 进程数量。
    'task_worker_num' => 4,
]);

$http->on('Request', function ($request, $response) use ($http) {
    echo "接收到了请求", PHP_EOL;
    $response->header('Content-Type', 'text/html; charset=utf-8');

    $http->task("发送邮件");
    $http->task("发送广播");
    $http->task("执行队列");

    $response->end('<h1>Hello Swoole. #' . rand(1000, 9999) . '</h1>');
});

//处理异步任务(此回调函数在task进程中执行)
//Task 事件是用于处理任务的，可以根据传递过来的 $data 内容进行处理。
$http->on('Task', function ($serv, $task_id, $reactor_id, $data) {
    $sec = rand(1, 5);
    echo "New AsyncTask[id={$task_id}] sleep sec: {$sec}" . PHP_EOL;
    sleep($sec);
    //返回任务执行的结果
    $serv->finish("{$data} -> OK");
});

//处理异步任务的结果(此回调函数在worker进程中执行)
//Finish 事件是监听任务结束，当执行的任务结束后，就会调用这个事件回调，可以进行后续的处理。如果你的任务没有后续的处理，那么我们也可以不去监听这个事件。
$http->on('Finish', function ($serv, $task_id, $data) {
    echo "AsyncTask[{$task_id}] Finish: {$data}" . PHP_EOL;
});

echo "服务启动", PHP_EOL;
$http->start();
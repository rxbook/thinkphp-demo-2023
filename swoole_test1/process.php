<?php
/**
 * swoole 子进程
 */
$process = new Swoole\Process(function (Swoole\Process $pro) {
    echo "输出内容"; //第二个参数如果是false,会将输出打印出来.如果是true则不会打印输出
    //exec方法执行一个外部程序,注意需要使用php进程的绝对路径
    //下面表示开启一个子进程,运行 php process.php
    $pro->exec("/Applications/MxSrvs/bin/php/bin/php", [__DIR__ . '/HTTP.php']);
}, false);

$pid = $process->start(); //输出子进程的pid
echo $pid . PHP_EOL;

swoole_process::wait(); //回收结束运行的子进程(子进程结束必须要执行wait进行回收，否则子进程会变成僵尸进程)

/*
 * 运行此程序后,使用 ps -ef | grep process.php ,会看到当前进程的id,此处结果是 6283
 * 然后 pstree -p 6283,会看到进程树:
 \-+= 06283 renxing php process.php
   \-+- 06284 renxing php HTTP.php
     \-+- 06285 renxing php HTTP.php
       |--- 06286 renxing php HTTP.php
       |--- 06287 renxing php HTTP.php
       |--- 06288 renxing php HTTP.php
       \--- 06289 renxing php HTTP.php

 */
